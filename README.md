# Andro

- `A case dispatcher for android-test test cases. Will dispatch testcases to devices dynamically and evenly`

- Be able to custom case execution on each case, if you want to.

- Solve deficiencies of [Spoon](https://github.com/square/spoon) and connectedAndroidTest

- Report using [Allure](https://github.com/allure-framework/allure2), lightweight and configurable


## How To

### Apply Andro to your android project:
```groovy

// First, add plugin depnedency
buildscript {
    repositories {
        mavenCentral()
    }

    dependencies {
        classpath 'com.glip.mobile:case-dispatcher:1.0-SNAPSHOT'
    }
}

// Apply the plugin
apply plugin: 'com.glip.mobile.plugin.case-dispatcher'

// Add report depencies, see https://github.com/TinkoffCreditSystems/allure-android/wiki/usage#configure
dependencies {
    androidTestCompile "ru.tinkoff.allure:allure-android:2.5.1@aar"
    androidTestCompile "ru.tinkoff.allure:allure-common:2.5.1"
    androidTestCompile "ru.tinkoff.allure:allure-model:2.5.1"
}

// Specific runner for Report
android {
    defaultConfig {
        testInstrumentationRunner "ru.tinkoff.allure.android.AllureAndroidRunner"
    }
}
```


### Run with gradle task
```sh
    # Find andro gradle task
    gradle tasks | grep Automation-test -A 10 | grep ispatch

    # And you will see tasks like <Flavor><BuildType>Dispatch, run it:
    gradle inhouseDebugDispatch

    # Also we support strategies, do below:
    gradle inhouseDebugDispatch -P strategy="dispatch"
    gradle inhouseDebugDispatch -P strategy="fullrun"

```

Then Andro will load cases and dispatch them to devices listed in output of `adb devices`
If not specified in dext, you will see report at
 `build/automation_report/<flavor, e.g. Inhouse>/<Buildtype, e.g. release>/` by default

 View Allure report:
 ```sh
    # install allure first
    brew install allure

    cd build/automation_report/<flavor, e.g. Inhouse>/<Buildtype, e.g. release>/json
    allure serve .
 ```

### Advance

You are allowed to specify variables like apk path
```groovy
// Specify extensions, feel free to modify or use the default configurations
dext {

    // You are not required to specify these, andro will find them automatedly
    classPath = "Your additional path where you want to load testcases from"
    apkPath = "Your additional apk"
    testApkPath = "Your additional test apk"
    reportDir = "Where you want to generate report at"

    // You can set it to false to avoid running assemble tasks befor dispatching, by default it's true
    reAssemble = false
}
```
