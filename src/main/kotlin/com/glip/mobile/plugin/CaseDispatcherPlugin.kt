package com.glip.mobile.plugin

import com.glip.mobile.plugin.ConfigurableVariables.Companion.DISPATCH_EXT_NAME
import com.glip.mobile.plugin.extensions.DispatchExt
import com.glip.mobile.plugin.extensions.createDispatchTasks
import com.glip.mobile.plugin.extensions.info
import com.glip.mobile.plugin.tasks.DispatchTask
import org.gradle.api.Plugin
import org.gradle.api.Project

class CaseDispatcherPlugin : Plugin<Project> {

    override fun apply(project: Project) {

        project.info("=============================")
        project.info("CaseDispatcherPlugin starts!")
        project.info("=============================")

        project.extensions.add(DISPATCH_EXT_NAME, DispatchExt())

        project.createDispatchTasks()
        project.afterEvaluate {
            project.info("Add several tasks: ")
            it.tasks.all { ta ->
                if (ta is DispatchTask) {
                    project.info("Automation Task name with config:")
                    project.info(ta.name)
                    project.info(ta.config.toString())
                }
            }
        }
    }
}
