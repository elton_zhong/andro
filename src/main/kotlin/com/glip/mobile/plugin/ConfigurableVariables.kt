package com.glip.mobile.plugin

import com.glip.andro.annotations.TestCase
import com.glip.mobile.plugin.tasks.DispatchTaskConfig
import com.glip.mobile.plugin.extensions.DispatchExt
import com.glip.mobile.plugin.extensions.Required
import com.glip.mobile.plugin.util.FileUtil
import org.apache.commons.lang3.Validate
import org.apache.logging.log4j.LogManager
import kotlin.reflect.full.findAnnotation
import kotlin.reflect.full.memberProperties

class ConfigurableVariables private constructor() {

    // Below are configurable properties, and they will be set in config, also you can configure them in extension
    var classpath = ""

    var appApkPath = ""
    var appApkPathOnDevice = "/data/local/tmp/com.glip.mobile"

    var testApkPath = ""
    var testApkPathOnDevice = "/data/local/tmp/com.glip.mobile.test"

    var appPackage = "com.glip.mobile"
    var testPackage = "com.glip.mobile.test"

    var runner = "ru.tinkoff.allure.android.AllureAndroidRunner"
    var reportDir = ""

    var projectName = "Glip"
    var flavor = ""
    var buildType = ""

    // Below are computed variables
    val xmlDir: String
        get() = FileUtil.join(reportDir, XML_REPORT_DIR_NAME)

    val fileLogDir: String
        get() = FileUtil.join(reportDir, FILE_LOG_DIR_NAME)
    val fileLogPath: String
        get() = FileUtil.join(fileLogDir, FILE_LOG_FILE_NAME)
    val screenshotsDir: String
        get() = FileUtil.join(reportDir, SCREEN_SHOTS_DIR_NAME)
    val allureDir: String
        get() = FileUtil.join(reportDir, ALLURE_REPORT_DIR_NAME)

    /**
     * Load config from gradle extension [ext]
     */
    fun loadExt(ext: DispatchExt) {
        logger.info("Reading properties from Extension: \n $ext")
        validateForExt(ext)
        doLoad(ext)
    }

    /**
     * Load config from dispatch task config [config],
     * which is computed and got from gradle android plugin
     */
    fun loadConfig(config: DispatchTaskConfig) {
        appApkPath = config.apkPath
        testApkPath = config.testApkPath
        buildType = config.buildType
        flavor = config.flavor
        classpath = config.classesPath
        reportDir = config.reportDir

        logger.info("Loading config \n$config")
    }

    private fun doLoad(ext: DispatchExt) {

        ext.classPath?.let { classpath = it }

        ext.apkPath?.let { appApkPath = it }
        ext.apkTo?.let { appApkPathOnDevice = it }

        ext.testApkPath?.let { testApkPath = it }
        ext.testApkTo?.let { testApkPathOnDevice = it }

        ext.appPackage?.let { appPackage = it }
        ext.testPackage?.let { testPackage = it }

        ext.reportDir?.let { reportDir = it }

        ext.projectName?.let { projectName = it }
        ext.flavor?.let { flavor = it }
        ext.buildType?.let { buildType = it }
        ext.runner?.let { runner = it }
    }

    private fun validateForExt(ext: DispatchExt) {
        val messageTemplate = "%s can not be empty," +
            " should set it in extension $DISPATCH_EXT_NAME"

        val format = { it: String ->
            String.format(messageTemplate, it)
        }

        DispatchExt::class.memberProperties.forEach {
            if (it.findAnnotation<Required>() != null) {
                Validate.notEmpty(it.get(ext) as String?, format(it.name))
            }
        }
    }

    companion object {
        private var instance: ConfigurableVariables? = null
            get() = field ?: let { field = ConfigurableVariables(); return@let field }

        // Below are constants:
        val ANOTATION_FOR_TESTCASE = TestCase::class.java
        const val ANOTATION_FOR_IGNORED_TESTCASE = "org.junit.Ignore"

        const val TASK_GROUP = "automation-test"
        const val TASK_DESC = "Dispatch all cases to local adb devices to run"

        const val DISPATCH_TASK_NAME = "Dispatch"
        const val DISPATCH_EXT_NAME = "dext"

        const val REPORT_DIR_NAME = "automation_report"
        const val XML_REPORT_DIR_NAME = "xml"
        const val ALLURE_REPORT_DIR_NAME = "json"
        const val FILE_LOG_DIR_NAME = "log"
        const val FILE_LOG_FILE_NAME = "all.log"
        const val SCREEN_SHOTS_DIR_NAME = "screenshots"

        const val PARA_DEVICE_UDID = "deviceUdid"

        fun get(): ConfigurableVariables {
            return instance!!
        }

        /**
         * Reconfigure configs, justify for gradle deamon
         */
        fun clear() {
            instance = null
        }

        private val logger = LogManager.getLogger(ConfigurableVariables::class.java)
    }
}
