package com.glip.mobile.plugin.container

import com.glip.mobile.plugin.exceptions.BaseException

class AnnotationNotFoundException : BaseException()
