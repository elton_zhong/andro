package com.glip.mobile.plugin.container

import com.glip.mobile.plugin.ConfigurableVariables.Companion.ANOTATION_FOR_IGNORED_TESTCASE
import com.glip.mobile.plugin.ConfigurableVariables.Companion.ANOTATION_FOR_TESTCASE
import com.glip.mobile.plugin.exceptions.CaseNotEnoughException
import com.glip.mobile.plugin.model.CaseWrapper
import com.glip.mobile.plugin.util.ClassPoolUtil
import javassist.CtClass
import org.apache.logging.log4j.LogManager

open class CasesContainer : Cloneable {

    private val remainCases = arrayListOf<CaseWrapper>()
    private val finishedCases = arrayListOf<CaseWrapper>()
    private val runningCases = arrayListOf<CaseWrapper>()

    fun getRemainCases(): ArrayList<CaseWrapper> {
        return remainCases
    }

    fun constructCases(classPath: String) {
        val classes = ClassPoolUtil.getInstance().loadClassesByClasspath(classPath)
        logger.info("Find ${classes.size} classes:")
        val tasks = classes
            .apply { forEach { logger.debug(it.name) } }
            .filter(::isTestCase)
            .also { logger.info("Find ${it.size} classes with Tag $ANOTATION_FOR_TESTCASE") }
            .filter(::isNotIgnored)
            .also { logger.info("Find ${it.size} classes not ignored:") }
            .apply { forEach { logger.info(it.name) } }
            .map { CaseWrapper(it, this) }

        remainCases.addAll(tasks)
    }

    @Synchronized
    fun getCase(): CaseWrapper {
        if (remainCases.isNotEmpty()) {
            val task = remainCases[0]

            remainCases.remove(task)

            return task
        }

        throw CaseNotEnoughException()
    }

    open fun finishCase(castTask: CaseWrapper) {
        runningCases.remove(castTask)
        finishedCases.add(castTask)
    }

    fun isAllCasesDispatched(): Boolean {
        return remainCases.isEmpty()
    }

    public override fun clone(): CasesContainer {
        val fork = CasesContainer()
        fork.remainCases.addAll(remainCases.map { it.clone(container = fork) })

        return fork
    }

    private fun isTestCase(clz: CtClass): Boolean {
        return clz.getAnnotation(ANOTATION_FOR_TESTCASE) != null
    }

    private fun isNotIgnored(clz: CtClass): Boolean {
        return clz.availableAnnotations.none { an ->
            an.toString().contains(ANOTATION_FOR_IGNORED_TESTCASE)
        }
    }

    companion object {
        private val logger = LogManager.getLogger(CasesContainer::class.java)
    }
}
