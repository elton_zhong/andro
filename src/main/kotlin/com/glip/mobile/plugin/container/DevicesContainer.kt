package com.glip.mobile.plugin.container

import com.glip.mobile.plugin.exceptions.BaseException
import com.glip.mobile.plugin.model.DeviceWrapper
import org.apache.commons.lang3.Validate
import org.apache.logging.log4j.LogManager
import java.util.concurrent.Semaphore

class DevicesContainer {
    val devices = arrayListOf<DeviceWrapper>()
    val availableDevices = arrayListOf<DeviceWrapper>()

    // Semaphore is needed to handle situations that devices is removed from devices list permanently
    private val semaphore = Semaphore(0)

    fun getDevice(): DeviceWrapper {
        logger.info("Acquire device starts")
        semaphore.acquire()
        return popDevice()
    }

    override fun toString(): String {
        return "[Devices ${devices.joinToString("\t")}]"
    }

    @Synchronized
    private fun popDevice(): DeviceWrapper {
        Validate.notEmpty(availableDevices)
        val device = availableDevices[0]

        if (availableDevices.remove(device)) {
            logger.info("Acquire device success, get ${device.udid}")
            return device
        }

        throw BaseException("Should never reach here")
    }

    @Synchronized
    fun releaseDevice(deviceWrapper: DeviceWrapper) {
        logger.info("Release device ${deviceWrapper.udid}")
        availableDevices.add(deviceWrapper)
        semaphore.release()
    }

    fun loadDevices(devicesList: List<DeviceWrapper>) {
        Validate.notEmpty(devicesList)

        devices.addAll(devicesList)
        availableDevices.addAll(devicesList)
        semaphore.release(devicesList.size)
    }

    fun deprecateDevice(deviceWrapper: DeviceWrapper) {
        devices.remove(deviceWrapper)
    }

    fun isDeviceAllDead(): Boolean {
        return devices.isEmpty()
    }

    companion object {
        private val logger = LogManager.getLogger(DevicesContainer::class.simpleName)
    }
}
