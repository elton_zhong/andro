package com.glip.mobile.plugin.core

import com.glip.mobile.plugin.container.CasesContainer
import com.glip.mobile.plugin.container.DevicesContainer
import com.glip.mobile.plugin.model.DeviceWrapper
import com.glip.mobile.plugin.model.decorators.ITestRun
import com.glip.mobile.plugin.service.DispatchService
import org.apache.logging.log4j.LogManager

/**
 * TODO: There should be interface for other executors
 */
class AsyncExecutor(
    devicesList: List<DeviceWrapper> = listOf(),
    private val devicesContainer: DevicesContainer = DevicesContainer(),
    val casesContainer: CasesContainer = CasesContainer()
) {
    private val dispatcher = DispatchService(devicesContainer)
    val testRuns: MutableList<ITestRun> = arrayListOf()
    val logger by lazy { LogManager.getLogger(this.toString())!! }

    init {
        if (devicesList.isNotEmpty()) {
            loadDevices(devicesList)
        }
    }

    override fun toString(): String {
        return "[Executor with devices $devicesContainer]"
    }

    fun waitTillCasesEnd() {
        dispatcher.stopAndWaitAllCasesFinish()
    }

    fun execWithoutBlocking(): MutableList<ITestRun> {
        testRuns.addAll(dispatchCases())
        return testRuns
    }

    fun printStatus() {
        logger.debug("Start to print status of $this")
        logger.debug("TestRuns:")
        testRuns.forEachIndexed { index, it ->
            logger.debug("$index Test run with device ${it.getDevice()} and case ${it.getCase()}")
            logger.debug(it.getTestResult().status)
        }
    }

    private fun loadDevices(devicesList: List<DeviceWrapper>) {
        devicesContainer.loadDevices(devicesList)
    }

    private fun dispatchCases(): MutableList<ITestRun> {
        while (!casesContainer.isAllCasesDispatched()) {
            val caseTask = casesContainer.getCase()
            dispatcher.dispatchCase(caseTask)
        }

        logger.info("Finish dispatching.")
        return dispatcher.testRuns
    }

    companion object {
        val logger = LogManager.getLogger(AsyncExecutor::class.java)!!
    }
}