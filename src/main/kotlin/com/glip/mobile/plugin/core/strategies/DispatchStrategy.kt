package com.glip.mobile.plugin.core.strategies

import com.glip.mobile.plugin.container.CasesContainer
import com.glip.mobile.plugin.container.DevicesContainer
import com.glip.mobile.plugin.core.AsyncExecutor

class DispatchStrategy : IStrategy {
    override fun getExecutors(devices: DevicesContainer, cases: CasesContainer): List<AsyncExecutor> {
        return listOf(AsyncExecutor(casesContainer = cases, devicesList = devices.devices))
    }
}
