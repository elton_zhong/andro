package com.glip.mobile.plugin.core.strategies

import com.glip.mobile.plugin.container.CasesContainer
import com.glip.mobile.plugin.container.DevicesContainer
import com.glip.mobile.plugin.core.AsyncExecutor

interface IStrategy {
    fun getExecutors(devices: DevicesContainer, cases: CasesContainer): List<AsyncExecutor>
}
