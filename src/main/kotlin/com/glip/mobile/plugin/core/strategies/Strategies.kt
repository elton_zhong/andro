package com.glip.mobile.plugin.core.strategies

import java.lang.UnsupportedOperationException

class Strategies {
    companion object {
        fun get(strategyProperty: String): IStrategy {
            return when (strategyProperty) {
                DISPATCH_PROPERTY_NAME -> DISPATCH
                FULLRUN_PROPERTY_NAME -> FULL_RUN
                else -> throw UnsupportedOperationException("Now only support $DISPATCH_PROPERTY_NAME and $FULLRUN_PROPERTY_NAME")
            }
        }

        val DISPATCH = DispatchStrategy()
        val FULL_RUN = FullRunStrategy()

        private const val DISPATCH_PROPERTY_NAME = "dispatch"
        private const val FULLRUN_PROPERTY_NAME = "fullrun"
    }
}