package com.glip.mobile.plugin.exceptions

import java.lang.Exception

open class BaseException : Exception {
    constructor() : super()
    constructor(message: String) : super(message)
}