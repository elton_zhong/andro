package com.glip.mobile.plugin.exceptions

class DeviceNotEnoughException(message: String) : BaseException(message = message)
