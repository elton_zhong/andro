package com.glip.mobile.plugin.extensions

import org.gradle.api.Project
import kotlin.reflect.full.memberProperties

fun Project.getDispatchExt(): DispatchExt {
    return this.extensions.getByType(DispatchExt::class.java)
}

class DispatchExt {

    var classPath: String? = null
    var apkPath: String? = null
    var testApkPath: String? = null
    var apkTo: String? = null
    var testApkTo: String? = null
    var appPackage: String? = null
    var testPackage: String? = null
    var projectName: String? = null
    var buildType: String? = null
    var flavor: String? = null

    // Useless.. @see https://stackoverflow.com/questions/35721234/force-gradle-to-produce-an-apk
    var reAssemble: Boolean = true
    var reportDir: String? = null
    var runner: String? = null

    override fun toString(): String {
        val sb = StringBuilder()
        DispatchExt::class.memberProperties.forEach {
            sb.append(it.name)
            sb.append("\tis\t")
            sb.append(it.get(this) ?: "Empty")
            sb.append("\n")
        }

        return sb.toString()
    }
}

/**
 * Means this field is required in extension
 */
annotation class Required
