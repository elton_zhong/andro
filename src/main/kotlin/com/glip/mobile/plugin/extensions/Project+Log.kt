package com.glip.mobile.plugin.extensions

import org.gradle.api.Project
/**
 * @author locke.peng on 3/18/18.
 */

private const val LOG_TEMPLATE = ":{}:{}"

fun Project.trace(log: String) {
    this.logger.trace(LOG_TEMPLATE, this.name, log)
}

fun Project.debug(log: String) {
    this.logger.debug(LOG_TEMPLATE, this.name, log)
}

fun Project.info(log: String) {
    this.logger.info(LOG_TEMPLATE, this.name, log)
}

fun Project.warn(log: String) {
    this.logger.warn(LOG_TEMPLATE, this.name, log)
}

fun Project.error(log: String) {
    this.logger.error(LOG_TEMPLATE, this.name, log)
}

fun Project.quiet(log: String) {
    this.logger.quiet(LOG_TEMPLATE, this.name, log)
}