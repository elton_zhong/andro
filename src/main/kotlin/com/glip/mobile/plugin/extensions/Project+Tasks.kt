package com.glip.mobile.plugin.extensions

import com.android.build.gradle.AppExtension
import com.android.build.gradle.api.ApkVariant
import com.glip.mobile.plugin.ConfigurableVariables.Companion.DISPATCH_TASK_NAME
import com.glip.mobile.plugin.ConfigurableVariables.Companion.REPORT_DIR_NAME
import com.glip.mobile.plugin.tasks.DispatchTaskConfig
import com.glip.mobile.plugin.tasks.DispatchTask
import com.glip.mobile.plugin.util.FileUtil
import org.gradle.api.Project
import java.io.File

fun Project.createDispatchTasks() {
    createAndroidTestTasks(this)
}

private fun Project.createDispatchTask(config: DispatchTaskConfig): DispatchTask {
    val task = project.tasks.create(
        "${config.variant}$DISPATCH_TASK_NAME",
        DispatchTask::class.java
    )

    task.config = config
    if (project.getDispatchExt().reAssemble) {
        logger.info("Need to assemble, will execute task ${config.assembleTaskName} before task ${task.name}")

        task.dependsOn(project.getTasksByName(config.assembleAndroidTestTaskName, false))
        task.dependsOn(project.getTasksByName(config.assembleTaskName, false))
    } else {
        logger.warn("Won't reassemble again befor ${task.name}.")
    }
    return task
}

/**
 * Because android.testVariants.all use listener mode, functions here might have potentail bug
 * What I rely on:
 *    1. applicationVariant is created after testVariant
 *    2. testVariant is less than testVariant
 * Or the tasks won't be created fully
 */
private fun createAndroidTestTasks(project: Project) {
    val appExt = project.extensions.getByType(AppExtension::class.java)

    appExt.testVariants.all { variant ->
        val config = DispatchTaskConfig.getOrCreateConfig(variant.flavorName, variant.buildType.name)
        config.testApkPath = getOutputApkPath(variant)
        config.testVariant = variant.name
        config.classesPath = project.getAndroidTestKotlinClassesPath(config.testVariant)
        config.reportDir = project.getReportDir(config.flavor, config.buildType)

        project.createDispatchTask(config)
    }

    appExt.applicationVariants.all { variant ->
        val config = DispatchTaskConfig.getConfig(variant.flavorName, variant.buildType.name)
        if (config != null) {
            config.apkPath = getOutputApkPath(variant)
        }
    }
}

private fun Project.getReportDir(flavor: String, buildType: String): String {
    return FileUtil.join(this.buildDir, REPORT_DIR_NAME, flavor, buildType).absolutePath
}

private fun getOutputApkPath(va: ApkVariant): String {
    if (va.outputs.isEmpty() || va.outputs.size > 1) {
        throw Exception("TODO: fix me! connenct author to fix it")
    }

    // Should already be here!
    return va.outputs
        .find { it != null }!!.outputFile.absolutePath
}

private fun Project.getAndroidTestKotlinClassesPath(testVariant: String): String {
    return buildDir.absolutePath + listOf("", "tmp", "kotlin-classes", testVariant).joinToString(File.separator)
}
