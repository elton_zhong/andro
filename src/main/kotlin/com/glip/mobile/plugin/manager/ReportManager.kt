package com.glip.mobile.plugin.manager

import com.glip.mobile.plugin.ConfigurableVariables
import com.glip.mobile.plugin.model.DeviceWrapper
import com.glip.mobile.plugin.container.DevicesContainer
import com.glip.mobile.plugin.util.AllureUtil
import com.glip.mobile.plugin.util.FileUtil
import org.apache.logging.log4j.LogManager
import java.io.File

class ReportManager(devicesContainer: DevicesContainer) {

    val allureReportDir: String = ConfigurableVariables.get().allureDir
    val devices: DevicesContainer = devicesContainer

    fun generateAllureReport() {
        devices.devices.forEach {
            val to = FileUtil.join(File(allureReportDir), it.udid).absolutePath
            try {
                pullReportFromDevice(it, to)
            } catch (e: Throwable) {
                logger.error("Can not pull report from device ${it.udid}", e)
            }
        }
        AllureUtil.mergeAllureResultsUnder(allureReportDir)
        logger.info("Generate allure jsons at $allureReportDir")
    }

    private fun pullReportFromDevice(device: DeviceWrapper, to: String) {
        logger.info("Pulling report from device ${device.udid} to $to")
        AllureUtil.pullAllureResults(device, to)
    }

    companion object {
        val logger = LogManager.getLogger(ReportManager::class.java)!!
    }
}
