package com.glip.mobile.plugin.manager

import com.glip.mobile.plugin.model.DeviceWrapper
import com.glip.mobile.plugin.container.CasesContainer
import com.glip.mobile.plugin.container.DevicesContainer
import com.glip.mobile.plugin.core.AsyncExecutor
import com.glip.mobile.plugin.core.strategies.IStrategy
import org.apache.commons.lang3.Validate
import org.apache.logging.log4j.LogManager
import java.util.concurrent.Future

open class RunnerManager {
    private val casesContainer = CasesContainer()
    val devicesContainer = DevicesContainer()
    val executors = arrayListOf<AsyncExecutor>()

    fun run(strategy: IStrategy) {
        executors.addAll(strategy.getExecutors(devicesContainer, casesContainer))
        executors.forEach {
            it.execWithoutBlocking()
        }

        executors.forEach {
            it.waitTillCasesEnd()
        }

        logger.info("Start to print status for all executors:")
        executors.forEach {
            it.printStatus()
        }
        val runs = executors.flatMap { it.testRuns }
        logger.info("Total ${runs.size} test runs.")
    }

    fun loadCases(classPath: String) {
        logger.info("Load classes from path: $classPath")

        casesContainer.constructCases(classPath)
    }

    fun loadDevices(devices: List<DeviceWrapper>) {
        Validate.notEmpty(devices)
        logger.info("Load ${devices.size} devices")
        devicesContainer.loadDevices(devices)
    }

    companion object {
        private val logger = LogManager.getLogger(RunnerManager::class.java)

        /**
         * Check unexpected result not happening in threads, such as device lost connection
         * If it do happens. throw the Exception
         */
        private fun checkCases(futures: ArrayList<Future<*>>) {
            logger.info("Cases are finished")
            logger.info("*********************************************")

            var i = 0
            var th: Throwable? = null

            // If there is exception in threads, throw it
            futures.forEach {
                if (it.isCancelled) {
                    logger.info("${++i} Test is canceled")
                    return@forEach
                }

                try {
                    it.get()
                    logger.info("${++i} Test ran normally")
                } catch (e: Throwable) {
                    th = e
                    logger.info("${++i} Test ran a strange result, connection to device might be broken")
                }
            }

            logger.info("*********************************************")

            if (th != null) {
                throw th as Throwable
            }
        }
    }
}
