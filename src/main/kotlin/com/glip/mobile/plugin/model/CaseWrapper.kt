package com.glip.mobile.plugin.model

import com.glip.andro.annotations.ReInstall
import com.glip.andro.annotations.TestCase
import com.glip.mobile.plugin.container.AnnotationNotFoundException
import com.glip.mobile.plugin.container.CasesContainer
import com.glip.mobile.plugin.util.ClassPoolUtil
import javassist.CtClass
import org.apache.logging.log4j.LogManager

open class CaseWrapper(val classPathOfCase: CtClass, var casesContainer: CasesContainer) {
    val name = classPathOfCase.name
    open val isNeedTobeReInstall: Boolean by lazy { reInstall != null }
    val testCase: TestCase by lazy {
        getAnnotation(TestCase::class.java)
    }
    open val reInstall: ReInstall? by lazy {
        try {
            getAnnotation(ReInstall::class.java)
        } catch (e: AnnotationNotFoundException) {
            null
        }
    }

    override fun toString(): String = name

    fun finish() {
        casesContainer.finishCase(this)
    }

    fun clone(container: CasesContainer): CaseWrapper {
        return CaseWrapper(classPathOfCase = classPathOfCase, casesContainer = container)
    }

    private fun <T : Annotation> getAnnotation(clz: Class<T>): T {
        return ClassPoolUtil.getInstance().getClassAnnotationInstance(classPathOfCase, clz)
    }

    companion object {
        private val logger = LogManager.getLogger(CaseWrapper::class.simpleName)
    }
}