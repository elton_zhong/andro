package com.glip.mobile.plugin.model

import com.android.builder.testing.api.DeviceConnector
import com.android.ddmlib.IShellEnabledDevice
import com.glip.mobile.plugin.ConfigurableVariables
import com.glip.mobile.plugin.util.AdbUtil
import com.glip.mobile.plugin.util.AllureUtil.Companion.setUpAllure
import org.apache.logging.log4j.LogManager

/**
 * TODO: use [com.android.builder.testing.ConnectedDeviceProvider] to overwrite all these logic
 */
open class DeviceWrapper(val deviceInstance: DeviceConnector) :
    IShellEnabledDevice by deviceInstance {

    val udid = deviceInstance.serialNumber

    open fun prepare() {
        uninstallPackagesIfAny()
        pushAppToDevice()
        installApp()
        setUpAllure(this)
    }

    open fun reInstall() {
        uninstallPackagesIfAny()
        installApp()
    }

    override fun toString(): String {
        return "[Device $udid]"
    }

    private fun uninstallPackagesIfAny() {
        try {
            uninstallPackages()
        } catch (ignored: Exception) {
            logger.debug("Uinstall package failed, device might not have loaded the packages before", ignored)
        }
    }

    private fun uninstallPackages() {
        AdbUtil.adbUninstall(udid, ConfigurableVariables.get().testPackage)
        AdbUtil.adbUninstall(udid, ConfigurableVariables.get().appPackage)
    }

    private fun pushAppToDevice() {
        AdbUtil.adbPush(
            udid,
            ConfigurableVariables.get().appApkPath,
            ConfigurableVariables.get().appApkPathOnDevice
        )
        AdbUtil.adbPush(
            udid,
            ConfigurableVariables.get().testApkPath,
            ConfigurableVariables.get().testApkPathOnDevice
        )
    }

    private fun installApp() {
        AdbUtil.adbInstall(udid, ConfigurableVariables.get().appApkPathOnDevice)
        AdbUtil.adbInstall(udid, ConfigurableVariables.get().testApkPathOnDevice)
    }

    companion object {
        val logger = LogManager.getLogger(DeviceWrapper::class.java)!!
    }
}
