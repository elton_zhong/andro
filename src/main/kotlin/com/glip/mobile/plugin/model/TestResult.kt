package com.glip.mobile.plugin.model

import java.util.concurrent.Future

class TestResult {
    lateinit var future: Future<*>
    var status = Status.INIT
    var exception: Throwable? = null

    fun isFinished(): Boolean {
        return status.isFinished()
    }

    fun onFinish() {
        if (future.isCancelled) {
            status = Status.SKIP
            return
        }

        try { future.get() } catch (e: Throwable) {
            exception = e
            status = Status.ERROR
            return
        }

        status = Status.SUCCESS
    }

    companion object {
        val NULL = TestResult()
    }

    enum class Status {
        INIT,
        SKIP,
        ERROR,
        SUCCESS;

        fun isFinished(): Boolean {
            return this != INIT
        }
    }
}
