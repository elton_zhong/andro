package com.glip.mobile.plugin.model

import com.android.builder.internal.testing.CustomTestRunListener
import com.android.ddmlib.testrunner.RemoteAndroidTestRunner
import com.glip.mobile.plugin.ConfigurableVariables
import com.glip.mobile.plugin.ConfigurableVariables.Companion.PARA_DEVICE_UDID
import com.glip.mobile.plugin.model.decorators.ITestRun
import com.glip.mobile.plugin.util.AdbUtil
import com.glip.mobile.plugin.util.LogUtil
import org.apache.logging.log4j.LogManager
import java.io.File

open class TestRun : ITestRun {
    lateinit var caseWrapper: CaseWrapper
    lateinit var deviceWrapper: DeviceWrapper

    @JvmField
    var result = TestResult.NULL

    override fun setDevice(deviceWrapper: DeviceWrapper) {
        this.deviceWrapper = deviceWrapper
    }

    override fun execute() {
        runCaseOnDevice()
    }

    override fun after() {
        caseWrapper.finish()
    }

    /**
     * Similar way to run caseWrapper like gradle connectedAndroidTest
     */
    private fun runCaseOnDevice(): RemoteAndroidTestRunner {
        val conf = ConfigurableVariables.get()

        val runner = RemoteAndroidTestRunner(
            conf.testPackage,
            conf.runner,
            deviceWrapper.deviceInstance
        )
        runner.setClassName(caseWrapper.name)

        val listener = CustomTestRunListener(
            "${deviceWrapper.udid}-${System.currentTimeMillis()}",
            conf.projectName,
            conf.flavor,
            LogUtil.getAdapter(LogManager.getLogger("${deviceWrapper.udid}_${caseWrapper.name}"))
        )
        listener.setReportDir(File(conf.xmlDir))

        runner.addInstrumentationArg(PARA_DEVICE_UDID, deviceWrapper.udid)
        runner.run(listener)
        return runner
    }

    @SuppressWarnings("This is for debug using")
    private fun runCaseOnDeviceUsingCmd() {
        AdbUtil.runTest(deviceWrapper.udid, caseWrapper.name)
    }

    override fun getCase(): CaseWrapper {
        return caseWrapper
    }

    override fun getDevice(): DeviceWrapper {
        return deviceWrapper
    }

    override fun getTestResult(): TestResult {
        return result
    }

    companion object {
        private val logger = LogManager.getLogger(TestRun::class.java)
    }
}
