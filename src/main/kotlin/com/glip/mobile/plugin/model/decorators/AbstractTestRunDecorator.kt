package com.glip.mobile.plugin.model.decorators

abstract class AbstractTestRunDecorator(open var run: ITestRun) : ITestRun by run {

    /**
     * This function should not be delegated
     */
    override fun run() {
        before()
        execute()
        after()
    }
}