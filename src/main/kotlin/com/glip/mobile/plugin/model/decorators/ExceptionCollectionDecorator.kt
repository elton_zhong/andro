package com.glip.mobile.plugin.model.decorators

import org.apache.logging.log4j.LogManager

open class ExceptionCollectionDecorator(run: ITestRun) : AbstractTestRunDecorator(run) {
    var beforeThrowable: Throwable? = null
    var executeThrowable: Throwable? = null
    var afterThrowable: Throwable? = null

    override fun before() {
        try {
            super.before()
        } catch (e: Throwable) {
            beforeThrowable = e

            // How to handle exception in before, remains TODO
            throw e
        }
    }

    override fun execute() {
        try {
            super.execute()
        } catch (e: Throwable) {
            logger.error(
                "Unexpected error when running caseWrapper ${getCase().name} on deviceWrapper ${getDevice().udid}")
            executeThrowable = e
        }
    }

    override fun after() {
        try {
            super.after()
        } catch (e: Throwable) {
            afterThrowable = executeThrowable

            // How to handle exception in after, remains TODO
            throw e
        }

        checkThrowables()
    }

    private fun checkThrowables() {
        if (executeThrowable != null) {
            logger.error(
                "Unexpected error when running caseWrapper ${getCase().name} on deviceWrapper ${getDevice().udid}")
            throw executeThrowable as Throwable
        }
    }

    companion object {
        private val logger = LogManager.getLogger(ExceptionCollectionDecorator::class.java)
    }
}
