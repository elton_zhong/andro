package com.glip.mobile.plugin.model.decorators

import com.glip.mobile.plugin.model.CaseWrapper
import com.glip.mobile.plugin.model.DeviceWrapper
import com.glip.mobile.plugin.model.TestResult

interface ITestRun {

    fun run() {
        before()
        execute()
        after()
    }

    fun before() {}
    fun after() {}
    fun execute() {}

    fun getCase(): CaseWrapper
    fun getDevice(): DeviceWrapper
    fun getTestResult(): TestResult

    fun setDevice(deviceWrapper: DeviceWrapper)
}