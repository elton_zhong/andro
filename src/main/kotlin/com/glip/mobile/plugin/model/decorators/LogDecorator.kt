package com.glip.mobile.plugin.model.decorators

import org.apache.logging.log4j.LogManager

open class LogDecorator(run: ITestRun) : AbstractTestRunDecorator(run) {
    override fun before() {
        logger.info("[Before] case ${getCase()} execution starts..")
        super.before()
        logger.info("[Before] case ${getCase()} execution ends..")
    }

    override fun execute() {
        logger.info("Device ${getDevice().udid} is running ${getCase().name}")
        super.execute()
        logger.info("Case ${getDevice().udid} ran on ${getCase().name} successfully")
    }

    override fun after() {
        logger.info("[After] case ${getCase()} execution starts..")
        super.after()
        logger.info("[After] case ${getCase()} execution ends..")
    }

    companion object {
        private val logger = LogManager.getLogger(LogDecorator::class.java)
    }
}
