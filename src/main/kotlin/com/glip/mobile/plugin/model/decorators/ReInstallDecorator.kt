package com.glip.mobile.plugin.model.decorators

import org.apache.logging.log4j.LogManager

open class ReInstallDecorator(run: ITestRun) : AbstractTestRunDecorator(run) {
    override fun before() {
        reInstallIfShould()
        super.before()
    }

    private fun reInstallIfShould() {
        if (getCase().isNeedTobeReInstall) {
            logger.info("Start to reinstall for case ${getCase()} and device ${getDevice()}")
            getDevice().reInstall()
        }
    }

    companion object {
        val logger = LogManager.getLogger(ReInstallDecorator::class.java)!!
    }
}
