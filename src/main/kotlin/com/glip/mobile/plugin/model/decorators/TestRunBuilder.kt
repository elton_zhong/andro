package com.glip.mobile.plugin.model.decorators

import com.glip.mobile.plugin.model.CaseWrapper
import com.glip.mobile.plugin.model.DeviceWrapper
import com.glip.mobile.plugin.model.TestResult
import com.glip.mobile.plugin.model.TestRun
import org.apache.logging.log4j.LogManager

class TestRunBuilder private constructor() {
    var run = TestRun()

    fun setDevice(deviceWrapper: DeviceWrapper): TestRunBuilder {
        run.deviceWrapper = deviceWrapper
        return this
    }

    fun setResult(result: TestResult): TestRunBuilder {
        run.result = result
        return this
    }

    fun build(): ITestRun {
        return ExceptionCollectionDecorator(LogDecorator(ReInstallDecorator(run)))
    }

    companion object {
        fun setCase(case: CaseWrapper): TestRunBuilder {
            val builder = TestRunBuilder()
            builder.run.caseWrapper = case
            return builder
        }

        private val logger = LogManager.getLogger(TestRunBuilder::class.java)
    }
}