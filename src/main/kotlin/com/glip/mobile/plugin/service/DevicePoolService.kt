package com.glip.mobile.plugin.service

import com.android.build.gradle.internal.LoggerWrapper
import com.android.builder.testing.ConnectedDeviceProvider
import com.glip.mobile.plugin.exceptions.DeviceNotEnoughException
import com.glip.mobile.plugin.model.DeviceWrapper
import com.glip.mobile.plugin.util.AdbUtil.Companion.getAdbPath
import joptsimple.internal.Strings
import org.apache.logging.log4j.LogManager
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

class DevicePoolService {
    companion object {
        val logger = LogManager.getLogger(DevicePoolService::class.java)!!
        var instance: DevicePoolService? = null
            get() = field ?: let { field = DevicePoolService(); return@let field }
            private set

        fun get(): DevicePoolService {
            return instance!!
        }
    }

    private var deviceProvider: ConnectedDeviceProvider
    lateinit var devices: List<DeviceWrapper>

    init {
        logger.info("Start to find devices from cmd `adb devices`")
        deviceProvider = ConnectedDeviceProvider(
            getAdbPath(),
            0,
            LoggerWrapper.getLogger(javaClass)
        )

        scanDevices()
        prepareDevices()
    }

    /**
     * Destroy device pool
     */
    fun destory() {
        logger.info("Start to destroy device pool")
        instance = null
        logger.info("Finish to destroy device pool")
    }

    private fun scanDevices() {

        deviceProvider.init()

        devices = deviceProvider.devices.map { DeviceWrapper(it) }

        logger.info(
            String.format(
                "Scan for devicesContainer, find: %s",
                Strings.join(devices.map { it.udid }, "\t")
            )
        )

        if (devices.isEmpty()) {
            throw DeviceNotEnoughException("No device conneted to this computer")
        }
    }

    /**
     * Install apks on device
     * Use thread to save time
     */
    private fun prepareDevices() {
        val futures = arrayListOf<Future<*>>()
        val executor = Executors.newCachedThreadPool()

        devices.forEach {
            futures.add(executor.submit {
                try {
                    it.prepare()
                } catch (e: Throwable) {
                    executor.shutdownNow()
                    throw e
                }
            })
        }

        executor.shutdown()
        executor.awaitTermination(1000L, TimeUnit.MINUTES)

        // Throw exception if any
        futures.forEach { it.get() }
    }
}
