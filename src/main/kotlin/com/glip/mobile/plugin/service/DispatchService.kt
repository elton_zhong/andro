package com.glip.mobile.plugin.service

import com.glip.mobile.plugin.container.DevicesContainer
import com.glip.mobile.plugin.model.CaseWrapper
import com.glip.mobile.plugin.model.DeviceWrapper
import com.glip.mobile.plugin.model.TestResult
import com.glip.mobile.plugin.model.decorators.ITestRun
import com.glip.mobile.plugin.model.decorators.TestRunBuilder
import org.apache.logging.log4j.LogManager
import java.lang.Exception
import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.TimeUnit

class DispatchService(private val devicesContainer: DevicesContainer) {

    val testRuns = arrayListOf<ITestRun>()
    private val executor by lazy { Executors.newFixedThreadPool(devicesContainer.devices.size) }

    fun dispatchCase(caseTask: CaseWrapper): ITestRun {
        val testResult = TestResult()
        val testRun = TestRunBuilder.setCase(caseTask).setResult(testResult).build()
        testRuns.add(testRun)

        testResult.future = executor.submit {
            fetchDeviceAndRun(testRun)
        }

        return testRun
    }

    private fun runTestWithDevice(testRun: ITestRun, device: DeviceWrapper) {
        try {
            testRun.setDevice(device)
            testRun.run()
        } catch (e: Exception) {
            logger.error("Due to This unexpected error, device will be deprecated and not used any more.", e)
            devicesContainer.deprecateDevice(device)

            shutdownIfNecessary()
            throw e
        }
    }

    private fun fetchDeviceAndRun(testRun: ITestRun) {
        val device = devicesContainer.getDevice()
        runTestWithDevice(testRun, device)
        devicesContainer.releaseDevice(device)
    }

    fun stopAndWaitAllCasesFinish() {
        executor.shutdown()
        executor.awaitTermination(1000L, TimeUnit.MINUTES)
        finishTestRuns()
    }

    private fun finishTestRuns() {
        testRuns.forEach {
            it.getTestResult().onFinish()
        }
    }

    private fun shutdownIfNecessary() {
        if (devicesContainer.isDeviceAllDead()) {
            logger.info("Now device alive now. Force service to quit")
            forceStop()
        }
    }

    private fun forceStop() {
        executor.shutdownNow().forEach {
            (it as Future<*>).cancel(true)
        }
    }

    companion object {
        private val logger = LogManager.getLogger(DispatchService::class.simpleName)
    }
}
