package com.glip.mobile.plugin.tasks

import com.glip.mobile.plugin.ConfigurableVariables
import com.glip.mobile.plugin.core.strategies.IStrategy
import com.glip.mobile.plugin.core.strategies.Strategies
import com.glip.mobile.plugin.extensions.getDispatchExt
import com.glip.mobile.plugin.util.LogUtil
import org.apache.logging.log4j.LogManager
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

open class DispatchTask : DefaultTask() {
    init {
        group = ConfigurableVariables.TASK_GROUP
        description = ConfigurableVariables.TASK_DESC
    }

    lateinit var config: DispatchTaskConfig
    lateinit var configurableVariables: ConfigurableVariables

    @TaskAction
    fun run() {
        before()
        TaskInvoker(configurableVariables, getStrategy()).run()
        after()
    }

    private fun getStrategy(): IStrategy {
        val strategyProperty = project.properties.get("strategy") ?: return Strategies.DISPATCH
        return Strategies.get(strategyProperty as String)
    }

    /**
     * Due to the gradle demon, should clean the configs
     * https://docs.gradle.org/current/userguide/gradle_daemon.html
     */
    private fun after() {
        ConfigurableVariables.clear()
    }

    /**
     * Prepare workspace and logger file
     */
    private fun before() {

        // Clear at before is more safer
        ConfigurableVariables.clear()
        configurableVariables = ConfigurableVariables.get()

        // Output logger to a temp file, avoid logger file being cleared by [TaskInvoker.prepareWorkspace]
        LogUtil.logToTempFile()

        configurableVariables.loadConfig(config)
        configurableVariables.loadExt(project.getDispatchExt())
    }

    companion object {
        val log = LogManager.getLogger(DispatchTask::class.java)!!
    }
}
