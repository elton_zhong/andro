package com.glip.mobile.plugin.tasks

import org.apache.commons.lang3.Validate
import java.lang.StringBuilder
import kotlin.reflect.full.memberProperties

/**
 * Config computed by project android variants
 */
class DispatchTaskConfig private constructor(val flavor: String, val buildType: String) {
    lateinit var apkPath: String
    lateinit var testApkPath: String
    lateinit var testVariant: String
    lateinit var classesPath: String
    lateinit var reportDir: String

    val variant: String
        get() = flavor + getCapitalizedString(buildType)

    val assembleAndroidTestTaskName: String
        get() = "assemble${getCapitalizedString(flavor)}${getCapitalizedString(buildType)}AndroidTest"
    val assembleTaskName: String
        get() = "assemble${getCapitalizedString(flavor)}${getCapitalizedString(buildType)}"

    init {
        all.add(this)
    }

    /**
     * TODO: Do not repeat yourself
     */
    override fun toString(): String {
        val sb = StringBuilder()
        DispatchTaskConfig::class.memberProperties.forEach {
            sb.append(it.name)
            sb.append("\tis\t")
            sb.append(it.get(this) ?: "Empty")
            sb.append("\n")
        }

        return sb.toString()
    }

    private fun getCapitalizedString(string: String): String {
        Validate.notEmpty(string)
        return string[0].toUpperCase() + string.slice(1 until string.length)
    }

    companion object {
        val all = arrayListOf<DispatchTaskConfig>()

        fun getConfig(flavorName: String, buildType: String) =
            all.find { it.flavor.equals(flavorName) && it.buildType.equals(buildType) }

        fun getOrCreateConfig(flavor: String, buildType: String): DispatchTaskConfig {
            return getConfig(flavor, buildType)
                ?: DispatchTaskConfig(flavor, buildType)
        }
    }
}