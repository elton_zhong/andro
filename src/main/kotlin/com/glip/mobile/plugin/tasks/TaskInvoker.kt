package com.glip.mobile.plugin.tasks

import com.glip.mobile.plugin.ConfigurableVariables
import com.glip.mobile.plugin.core.strategies.IStrategy
import com.glip.mobile.plugin.manager.ReportManager
import com.glip.mobile.plugin.manager.RunnerManager
import com.glip.mobile.plugin.service.DevicePoolService
import com.glip.mobile.plugin.util.FileUtil
import com.glip.mobile.plugin.util.LogUtil
import org.apache.logging.log4j.LogManager
import java.io.File
import java.lang.RuntimeException

class TaskInvoker(private val configurableVariables: ConfigurableVariables, private val strategy: IStrategy) {
    lateinit var reportManager: ReportManager
    lateinit var manager: RunnerManager
    private lateinit var devicePoolService: DevicePoolService

    fun run() {
        before()
        val classpath = configurableVariables.classpath
        manager = RunnerManager()

        manager.loadCases(classpath)
        manager.loadDevices(devicePoolService.devices)
        manager.run(strategy)
        reportManager = ReportManager(manager.devicesContainer)

        after()
    }

    /**
     * Due to the gradle demon, should clean the configs
     * https://docs.gradle.org/current/userguide/gradle_daemon.html
     */
    private fun after() {
        logger.info("Task finished. Please go to ${configurableVariables.reportDir} to view reports")
        reportManager.generateAllureReport()

        devicePoolService.destory()
    }

    /**
     * Prepare workspace and logger file
     */
    private fun before() {
        logger.info("You are running task with strategy $strategy")
        prepareWorkspace()

        // Change file
        LogUtil.changeFileLoggerOutputTo(configurableVariables.fileLogPath)

        // Init instance for devicePoolService
        devicePoolService = DevicePoolService.instance!!
    }

    private fun prepareWorkspace() {
        safeCheckReportDir()
        FileUtil.remakeDirUnderPath(File(configurableVariables.reportDir))
        FileUtil.remakeDirUnderPath(File(configurableVariables.xmlDir))
        FileUtil.remakeDirUnderPath(File(configurableVariables.screenshotsDir))
        FileUtil.remakeDirUnderPath(File(configurableVariables.fileLogDir))
        FileUtil.remakeDirUnderPath(File(configurableVariables.allureDir))
    }

    private fun safeCheckReportDir() {
        val dir = configurableVariables.reportDir
        if (dir.endsWith(configurableVariables.buildType) && configurableVariables.buildType.isNotBlank()) {
            return
        }
        throw RuntimeException(
            "Safe checking...Your report dir $dir is not safe." +
                " Please contact author (This feature will be removed in future)"
        )
    }

    companion object {
        val logger = LogManager.getLogger(DispatchTask::class.java)!!
    }
}
