package com.glip.mobile.plugin.util

import com.glip.mobile.plugin.ConfigurableVariables
import com.glip.mobile.plugin.util.AllureUtil.Companion.ALLURE_DIR_ON_DEVICE
import joptsimple.internal.Strings
import org.apache.logging.log4j.LogManager
import java.io.File

private val logger = LogManager.getLogger("AdbUtil")

/**
 * TODO: delete me! duplicate wheels
 */
class AdbUtil {

    companion object {

        fun getAdbPath(): File {
            return File(CommandLineUtil.runCmdAndGetString(AdbCommands.WHICH_ADB))
        }

        fun getDevicesConnected(): List<String> {
            val output = CommandLineUtil.runCmd(AdbCommands.LIST_DEVICES)
            return parseOutputToDevicesList(output)
        }

        fun runTest(deviceUdid: String, classPath: String) {
            CommandLineUtil.runCmd(AdbCommands.RUN_TEST, deviceUdid, classPath)
        }

        fun adbPush(deviceUdid: String, sourcePath: String, target: String) {
            CommandLineUtil.runCmd(AdbCommands.ADB_PUSH, deviceUdid, sourcePath, target)
        }

        fun adbInstall(deviceUdid: String, target: String) {
            CommandLineUtil.runCmd(AdbCommands.ADB_INSTALL, deviceUdid, target)
        }

        fun adbPull(deviceUdid: String, from: String, to: String) {
            CommandLineUtil.runCmd(AdbCommands.ADB_PULL, deviceUdid, from, to)
        }

        fun adbUninstall(deviceUdid: String, target: String) {
            CommandLineUtil.runCmd(String.format(AdbCommands.ADB_UNINSTALL, deviceUdid, target))
        }

        fun adbSetupAllure(deviceUdid: String) {
            CommandLineUtil.runCmd(AdbCommands.ADB_RM_RF, deviceUdid, ALLURE_DIR_ON_DEVICE)
            CommandLineUtil.runCmd(AdbCommands.ADB_MAKDIR, deviceUdid, ALLURE_DIR_ON_DEVICE)
        }

        private fun parseOutputToDevicesList(output: Array<String>): List<String> {
            return output
                .filter { out -> !Strings.isNullOrEmpty(out) }
                .filter { out -> !out.contains("List of devices attached") }
                .map { it.split("\t").get(0) }
        }
    }
}

class AdbCommands {
    companion object {
        val LIST_DEVICES =
            "adb devices"

        val RUN_TEST =
            "adb -s %s shell am instrument -w -r -e debug false -e class %s " +
                "${ConfigurableVariables.get().testPackage}/${ConfigurableVariables.get().runner}"

        val ADB_PUSH =
            "adb -s %s push %s %s"

        val ADB_INSTALL =
            "adb -s %s shell pm install -t -r \"%s\""

        val ADB_UNINSTALL =
            "adb -s %s shell pm uninstall %s"

        val WHICH_ADB =
            "which adb"

        val ADB_RM_RF =
            "adb -s %s shell rm -rf %s"

        val ADB_MAKDIR =
            "adb -s %s shell mkdir -p %s"

        val ADB_PULL =
            "adb -s %s pull %s %s"
    }
}
