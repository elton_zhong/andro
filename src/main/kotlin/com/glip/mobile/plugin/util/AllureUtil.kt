package com.glip.mobile.plugin.util

import com.glip.mobile.plugin.model.DeviceWrapper
import java.io.File

class AllureUtil {
    companion object {
        const val ALLURE_DIR_ON_DEVICE = "/sdcard/allure-results"
        fun setUpAllure(device: DeviceWrapper) {
            AdbUtil.adbSetupAllure(device.udid)
        }

        fun pullAllureResults(device: DeviceWrapper, to: String) {
            AdbUtil.adbPull(device.udid, ALLURE_DIR_ON_DEVICE, to)
        }

        fun mergeAllureResultsUnder(dirPath: String) {
            val dir = File(dirPath)
            dir.listFiles().forEach {
                if (it.isDirectory) {
                    FileUtil.moveFilesUnderDirToDir(it, dir)
                    FileUtil.removeDirectoryRecursively(it)
                }
            }
        }
    }
}
