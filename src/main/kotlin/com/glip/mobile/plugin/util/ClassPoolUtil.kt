package com.glip.mobile.plugin.util

import com.glip.andro.annotations.ReInstall
import com.glip.andro.annotations.TestCase
import com.glip.mobile.plugin.container.AnnotationNotFoundException
import javassist.ClassClassPath
import javassist.ClassPool
import javassist.CtClass
import javassist.NotFoundException
import joptsimple.internal.Strings
import java.io.File

class ClassPoolUtil {
    companion object {
        private val INSTANCE by lazy { ClassPoolUtil() }

        fun getInstance(): ClassPoolUtil {
            return INSTANCE
        }
    }

    private val classPool = ClassPool()
    private val ANNOTATIONS = listOf<Class<*>>(TestCase::class.java, ReInstall::class.java)

    init {
        loadAnnotations()
    }

    /**
     * Load classes from classpath, can be invoked multi times
     */
    fun loadClassesByClasspath(classpath: String): ArrayList<CtClass> {
        val ctClasses = arrayListOf<CtClass>()

        classPool.appendClassPath(classpath)
        val classes = findAllClassesUnderClasspath(classpath)
        for (clazz in classes) {
            val classObj = try {
                classPool.get(clazz)
            } catch (e: NotFoundException) {
                continue
            }

            ctClasses.add(classObj)
        }

        return ctClasses
    }

    private fun findAllClassesUnderClasspath(classpath: String): List<String> {
        val root = File(classpath)

        return getClassesUnder(root, listOf())
    }

    @SuppressWarnings
    fun <T : Annotation> getClassAnnotationInstance(classPathOfCase: CtClass, clz: Class<T>): T {
        val ano = classPathOfCase.getAnnotation(clz)
        if (ano == null) {
            throw AnnotationNotFoundException()
        }
        return ano as T
    }

    private fun loadAnnotations() {
        ANNOTATIONS.forEach(::addClass)
    }

    private fun addClass(clz: Class<*>) {
        classPool.appendClassPath(ClassClassPath(clz))
    }

    private fun getClassesUnder(root: File, prefixes: List<String>): List<String> {
        if (root.isFile) {
            return if (checkFileIsJavaClass(root)) {
                listOf(constructClassPath(root.name, prefixes))
            } else {
                listOf()
            }
        }

        val classesList = ArrayList<String>()

        val nextPrefixes = ArrayList<String>()
        nextPrefixes.addAll(prefixes)
        nextPrefixes.add(root.name)

        for (f in root.listFiles()) {
            classesList.addAll(getClassesUnder(f, nextPrefixes))
        }

        return classesList
    }

    private fun constructClassPath(className: String, prefixes: List<String>): String {
        val list: ArrayList<String> = ArrayList(prefixes)

        // Get class name without .class suffix
        list.add(className.split(".")[0])

        // Get full class name com.glip.SomeClass according to file path combined of dirs
        return Strings.join(list.subList(1, list.size), ".")
    }

    private fun checkFileIsJavaClass(f: File): Boolean {
        val classSuffix = ".class"
        return f.name.endsWith(classSuffix)
    }
}
