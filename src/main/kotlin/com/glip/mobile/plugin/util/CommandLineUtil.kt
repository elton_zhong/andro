package com.glip.mobile.plugin.util

import joptsimple.internal.Strings
import org.apache.logging.log4j.LogManager
import java.io.BufferedReader
import java.io.InputStreamReader
import java.io.Reader

class CommandLineUtil {
    companion object {
        private val logger = LogManager.getLogger(CommandLineUtil::class.java)

        fun runCmd(cmdTemplate: String, vararg args: String): Array<String> {
            val cmd = String.format(cmdTemplate, *args)
            logger.info("Running command $cmd..")
            val process = Runtime.getRuntime().exec(cmd)
            val output = getOutput(process)

            if (checkCmdToExitNormallyWithBlocking(process)) {
                return output
            } else {
                logger.error("Command $cmd fails", cmd)
                throw java.lang.Exception("Command $cmd fails" + Strings.join(output, "\n"))
            }
        }

        fun runCmdAndGetString(cmdTemplate: String, vararg args: String): String {
            return Strings.join(runCmd(cmdTemplate, *args), "\n")
        }

        private fun checkCmdToExitNormallyWithBlocking(process: Process): Boolean {
            return process.waitFor() == 0
        }

        private fun getOutput(process: Process): Array<String> {
            return BufferedReader(InputStreamReader(process.inputStream) as Reader)
                .lines()
                .map { logger.info(it); it }
                .toArray { it -> arrayOfNulls<String>(it) }
        }
    }
}