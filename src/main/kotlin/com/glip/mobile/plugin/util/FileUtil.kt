package com.glip.mobile.plugin.util

import com.android.utils.FileUtils
import org.apache.logging.log4j.LogManager
import java.io.File

class FileUtil {
    companion object {
        val logger = LogManager.getLogger(FileUtil::class.java)

        /**
         * Will remove [f] first, and then create [dir] recursively
         * Notice that [dir] can be empty string, and will just be ignored
         */
        fun remakeDirUnderPath(f: File, vararg dir: String): String {
            removeDirectoryRecursively(f)
            val file = join(f, *dir)
            makdirRecursively(file)
            return file.absolutePath
        }

        fun makdirRecursively(f: File) {
            val cmd = "mkdir -p %s"
            CommandLineUtil.runCmdAndGetString(cmd, f.absolutePath)
        }

        fun removeDirectoryRecursively(file: File) {
            val cmd = "rm -rf %s"
            CommandLineUtil.runCmdAndGetString(cmd, file.absolutePath)
        }

        fun createFileIfFileNotExist(file: File) {
            makdirRecursively(file.parentFile)
            file.createNewFile()
        }

        /**
         * Join file and [dir], if [dir] is empty, will just ignore
         */
        fun join(f: File, vararg dir: String): File {
            return FileUtils.join(f, *dir)
        }

        fun join(vararg dir: String): String {
            return FileUtils.join(dir.filter { it.isNotBlank() })
        }

        fun moveFilesUnderDirToDir(dir: File, newPath: File) {
            dir.listFiles().forEach {
                if (it.isFile) {
                    move(it, join(newPath, it.name).absolutePath)
                }
            }
        }

        fun move(file: File, newPath: String) {
            if (!file.exists()) {
                logger.error("File ${file.absolutePath} not exists")
                throw Exception("Move ${file.absolutePath} to $newPath fails")
            }

            file.renameTo(File(newPath))
            logger.info("Move file ${file.absolutePath} to $newPath")
        }
    }
}
