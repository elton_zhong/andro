package com.glip.mobile.plugin.util

import com.android.utils.ILogger
import org.apache.logging.log4j.LogManager
import org.apache.logging.log4j.Logger
import java.io.File

class LogUtil {
    companion object {
        private const val TEMP_PREFIX = "TEMP"

        // Log file name, see log4j2.xml
        var logPath = "\${sys:logFilename}"
        private const val LOG_FILE_NAME_PROPERTY = "logFilename"

        private val logger = LogManager.getLogger(LogUtil::class.java)

        /**
         * Change file logger output to [path]
         * Workaround for create logger file correctly: Log4j2 will create logger file with file appender
         * See on stackoverflow https://stackoverflow.com/questions/21083834/load-log4j2-configuration-file-programmatically/21087859
         * See log4j2.xml in src/main/resources
         */
        fun changeFileLoggerOutputTo(path: String) {
            logger.info("Change logger file outputs to $path")

            if (!File(logPath).exists()) {
                logger.info("Origin file $logPath does not exist")

                // Create new file with [path]
                FileUtil.createFileIfFileNotExist(File(path))
            } else {

                // Before changing, logger will output to logPath, so mv it first
                FileUtil.move(File(logPath), path)
            }

            System.setProperty(LOG_FILE_NAME_PROPERTY, path)
            val ctx = LogManager.getContext(false) as org.apache.logging.log4j.core.LoggerContext
            ctx.reconfigure()
            logPath = path
        }

        fun getAdapter(logger: Logger): ILogger {
            return object : ILogger {
                override fun warning(msgFormat: String?, vararg args: Any?) {
                    logger.warn(String.format(msgFormat!!, *args))
                }

                override fun info(msgFormat: String?, vararg args: Any?) {
                    logger.info(String.format(msgFormat!!, *args))
                }

                override fun error(t: Throwable?, msgFormat: String?, vararg args: Any?) {
                    logger.error(String.format(msgFormat!!, *args), t)
                }

                override fun verbose(msgFormat: String?, vararg args: Any?) {
                    logger.info(String.format(msgFormat!!, *args))
                }
            }
        }

        /**
         * Due to the gradle deamon, the logger file will be created to last task's logfile
         * When executing gradle tasks, task will clean the old file output first.
         * So we need to mv this old file to some temp file, and after cleaning, mv this temp file to the output file
         */
        fun logToTempFile() {
            LogUtil.changeFileLoggerOutputTo(File.createTempFile(TEMP_PREFIX, null).absolutePath)
        }
    }
}