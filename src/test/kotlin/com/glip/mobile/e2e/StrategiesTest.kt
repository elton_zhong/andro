package com.glip.mobile.e2e

import com.glip.mobile.plugin.ConfigurableVariables
import com.glip.mobile.plugin.core.strategies.Strategies
import com.glip.mobile.plugin.tasks.TaskInvoker
import com.glip.mobile.testlib.factory.ConfigFactory
import org.junit.BeforeClass
import org.junit.Test

class StrategiesTest {

    companion object {
        private val factory = ConfigFactory()

        @JvmStatic
        @BeforeClass
        fun prepare() {
            factory.prepareWorkspace()
        }
    }

    @Test
    fun `smoke test that dispatch strategy goes well`() {
        val conf = ConfigurableVariables.get()
        conf.loadConfig(factory.getConfig())
        TaskInvoker(conf, Strategies.DISPATCH).run()
    }

    @Test
    fun `smoke Test that full run strategy goes well`() {
        val conf = ConfigurableVariables.get()
        conf.loadConfig(factory.getConfig())
        TaskInvoker(conf, Strategies.FULL_RUN).run()
    }
}
