package com.glip.mobile.plugin

import com.glip.mobile.plugin.extensions.DispatchExt
import com.glip.mobile.plugin.tasks.DispatchTaskConfigTest
import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import java.lang.IllegalArgumentException
import java.lang.NullPointerException

class ConfigurableVariablesTest {
    private val configurableVariables = ConfigurableVariables.get()

    fun prepareBasicExt(): DispatchExt {
        val ext = DispatchExt()

        ext.apkPath = "apkPath"
        ext.testApkPath = "testApkPath"
        ext.classPath = "testClasspath"
        ext.reportDir = "testReportdir"
        ext.buildType = "build"
        ext.flavor = "flavor"
        return ext
    }

    @Test
    fun testForLoadBasicExt() {
        val ext = prepareBasicExt()
        configurableVariables.loadExt(ext)

        Assert.assertEquals(configurableVariables.classpath, ext.classPath)
        Assert.assertEquals(configurableVariables.appApkPath, ext.apkPath)
        Assert.assertEquals(configurableVariables.testApkPath, ext.testApkPath)
        Assert.assertEquals(configurableVariables.reportDir, ext.reportDir)
    }

    @Test
    fun testForLoadFullExt() {
        val ext = prepareBasicExt()
        ext.appPackage = "testApkPackage"
        ext.apkTo = "testApkPackage"
        ext.testPackage = "testApkPackage"
        ext.testApkTo = "testApkPackage"
        ext.flavor = "testFlavor"
        ext.projectName = "prj"
        configurableVariables.loadExt(ext)

        Assert.assertEquals(configurableVariables.appPackage, ext.appPackage)
        Assert.assertEquals(configurableVariables.appApkPathOnDevice, ext.apkTo)
        Assert.assertEquals(configurableVariables.testPackage, ext.testPackage)
        Assert.assertEquals(configurableVariables.testApkPathOnDevice, ext.testApkTo)
        Assert.assertEquals(configurableVariables.flavor, ext.flavor)
        Assert.assertEquals(configurableVariables.projectName, ext.projectName)
    }

    @Test(expected = NullPointerException::class)
    @Ignore("No parameter is required now")
    fun testForNotEnoughParameters() {
        val ext = prepareBasicExt()

        ext.reportDir = null
        configurableVariables.loadExt(ext)
    }

    @Test(expected = IllegalArgumentException::class)
    @Ignore("No parameter is required now")
    fun testForEmptyParameters() {
        val ext = prepareBasicExt()

        ext.reportDir = ""
        configurableVariables.loadExt(ext)
    }

    @Test
    fun testForLoadConfig() {
        val config = DispatchTaskConfigTest.createConfig()
        configurableVariables.loadConfig(config)

        Assert.assertEquals(config.apkPath, configurableVariables.appApkPath)
        Assert.assertEquals(config.testApkPath, configurableVariables.testApkPath)
        Assert.assertEquals(config.classesPath, configurableVariables.classpath)
        Assert.assertEquals(config.flavor, configurableVariables.flavor)
        Assert.assertEquals(config.buildType, configurableVariables.buildType)
        Assert.assertEquals(config.reportDir, configurableVariables.reportDir)
    }

    @Test
    fun testForComputedDirs() {
        configurableVariables.reportDir = "/data/test"
        Assert.assertEquals(
            "/data/test/${ConfigurableVariables.XML_REPORT_DIR_NAME}",
            configurableVariables.xmlDir
        )
        Assert.assertEquals(
            "/data/test/${ConfigurableVariables.SCREEN_SHOTS_DIR_NAME}",
            configurableVariables.screenshotsDir
        )
        Assert.assertEquals(
            "/data/test/${ConfigurableVariables.FILE_LOG_DIR_NAME}",
            configurableVariables.fileLogDir
        )

        Assert.assertEquals(
            "${configurableVariables.fileLogDir}/${ConfigurableVariables.FILE_LOG_FILE_NAME}",
            configurableVariables.fileLogPath
        )
    }

    @Test
    fun testForConfigClear() {
        val ins1 = ConfigurableVariables.get()
        ConfigurableVariables.clear()
        val ins2 = ConfigurableVariables.get()
        Assert.assertNotEquals(ins1, ins2)
    }
}
