package com.glip.mobile.plugin.extensions

import org.junit.Assert
import org.junit.Test

class ExtTest {

    @Test
    fun testDispatchExt() {
        val dis = DispatchExt()
        Assert.assertNotNull(dis.toString())
        Assert.assertFalse(dis.toString().isEmpty())
        Assert.assertFalse(dis.toString().isBlank())

        dis.apkPath = "testforapkpath"

        Assert.assertTrue(dis.toString().contains(dis.apkPath!!))
    }
}