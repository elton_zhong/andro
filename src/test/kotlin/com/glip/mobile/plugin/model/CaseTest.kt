package com.glip.mobile.plugin.model

import com.glip.mobile.plugin.container.CasesContainer
import com.glip.mobile.testlib.factory.ConfigFactory
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CaseTest {
    lateinit var container: CasesContainer

    @Before
    fun prepare() {
        container = CasesContainer()
        container.constructCases(classPath = ConfigFactory.CLASSESPATH_DIR)
    }

    @Test
    fun `Test that casewrapper get a testcase annotation`() {
        val case = container.getCase()
        Assert.assertNotNull(case.testCase.desc)
        Assert.assertNotNull(case.testCase.id)
        Assert.assertNotNull(case.testCase.url)
    }

    @Test
    fun `Test that casewrapper need to reinstall property`() {

        val case = container.getRemainCases().find { it.name.contains("MTR_3") }!!
        Assert.assertNotNull(case.reInstall)
        Assert.assertEquals(case.reInstall!!.value, "install what?")
        Assert.assertEquals(case.isNeedTobeReInstall, true)
    }

    @Test
    fun `Test casewrapper which doesn't need to reinstall`() {

        val case = container.getRemainCases().find { it.name.contains("MTR_1") }!!
        Assert.assertNull(case.reInstall)
        Assert.assertEquals(case.isNeedTobeReInstall, false)
    }
}
