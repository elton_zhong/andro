package com.glip.mobile.plugin.model

import com.glip.mobile.plugin.container.CasesContainer
import com.glip.mobile.plugin.model.decorators.AbstractTestRunDecorator
import com.glip.mobile.plugin.model.decorators.ExceptionCollectionDecorator
import com.glip.mobile.plugin.model.decorators.ITestRun
import com.glip.mobile.plugin.model.decorators.LogDecorator
import com.glip.mobile.plugin.model.decorators.ReInstallDecorator
import com.glip.mobile.plugin.model.decorators.TestRunBuilder
import com.glip.mobile.testlib.factory.CaseFactory
import com.glip.mobile.testlib.factory.ConfigFactory
import com.glip.mobile.testlib.factory.DeviceFactory
import com.nhaarman.mockitokotlin2.doNothing
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.never
import com.nhaarman.mockitokotlin2.spy
import org.junit.Assert
import org.junit.Ignore
import org.junit.Test
import org.mockito.Mockito.verify

class TestRunTest {

    private val case = CaseFactory().getCase()
    private val device = DeviceFactory().getDevice()
    private val testResult = TestResult()

    @Test
    fun `Somke test for Builder build`() {
        val run = TestRunBuilder.setCase(case).build()
        Assert.assertNotNull(run)
        Assert.assertTrue(run is AbstractTestRunDecorator)
    }

    @Test
    fun `Test that builder set case and test result successfully`() {
        val run = TestRunBuilder.setCase(case).setDevice(device).setResult(result = testResult).build()
        Assert.assertTrue(run.getTestResult() == testResult)
        Assert.assertTrue(run.getCase() == case)
        Assert.assertTrue(run.getDevice() == device)
    }

    @Test
    fun `Test that run can set device after build`() {
        val run = TestRunBuilder.setCase(case).setResult(result = testResult).build()
        run.setDevice(device)
        Assert.assertTrue(run.getDevice() == device)
    }

    @Test
    fun `Test that test run is wrapped with decorators`() {
        val builder = TestRunBuilder.setCase(case).setResult(result = testResult)
        val originRun = builder.run
        val producedRun = builder.build() as AbstractTestRunDecorator
        Assert.assertNotEquals(originRun, producedRun)
        Assert.assertTrue(producedRun is ExceptionCollectionDecorator)
        Assert.assertTrue(producedRun.run is LogDecorator)
        Assert.assertTrue((producedRun.run as AbstractTestRunDecorator).run is ReInstallDecorator)
    }

    @Test
    fun `Test that methods in test run with decorators are ran`() {
        val builder = getMockRunBuilder()
        val spiedOriginRun = builder.run

        val producedRun = builder.build()
        producedRun.setDevice(device)
        producedRun.run()

        verify(spiedOriginRun).before()
        verify(spiedOriginRun).after()
        verify(spiedOriginRun).execute()
    }

    @Test
    fun `Decorators functions are ran`() {
        val delegatedTo = getMockTestRun()
        val decorator = LogDecorator(delegatedTo)
        val spiedDecorator = spy(decorator)

        spiedDecorator.run()

        verify(delegatedTo).execute()
        verify(delegatedTo).after()
        verify(delegatedTo).before()

        verify(spiedDecorator).execute()
        verify(spiedDecorator).after()
        verify(spiedDecorator).before()
    }

    @Test
    fun `wrapped ran fuction in decorators are not ran`() {
        val delegatedTo = getMockTestRun()
        val decorator = LogDecorator(delegatedTo)
        val spiedDecorator = spy(decorator)

        spiedDecorator.run()

        verify(spiedDecorator).run()
        verify(delegatedTo, never()).run()
    }

    @Test
    @Ignore("Fixme")
    fun `Case which need to be reinstalled will be reinstalled`() {
        val container = CasesContainer()
        container.constructCases(classPath = ConfigFactory.CLASSESPATH_DIR)
        val run = TestRunBuilder
            .setCase(
                container.getRemainCases()
                    .find { it.name.contains("MTR_3") }!!
            ).build()
        val runDecorator = ((run as AbstractTestRunDecorator).run as AbstractTestRunDecorator)
        Assert.assertTrue(runDecorator.run is ReInstallDecorator)
        runDecorator.run = mock<ReInstallDecorator>()
        run.run()
        verify(runDecorator.run).before()
    }

    private fun getMockTestRun(): ITestRun {
        val builder = getMockRunBuilder()
        val producedRun = builder.build()
        producedRun.setDevice(device)
        return spy(producedRun)
    }

    private fun getMockRunBuilder(): TestRunBuilder {
        val builder = TestRunBuilder.setCase(case).setResult(testResult)
        val spiedRun = spy(builder.run)
        builder.run = spiedRun
        doNothing().`when`(spiedRun).execute()
        return builder
    }
}