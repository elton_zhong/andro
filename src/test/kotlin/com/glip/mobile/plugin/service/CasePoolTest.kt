package com.glip.mobile.plugin.service

import com.glip.mobile.plugin.container.CasesContainer
import com.glip.mobile.plugin.exceptions.CaseNotEnoughException
import com.glip.mobile.plugin.model.CaseWrapper
import com.glip.mobile.testlib.factory.ConfigFactory.Companion.CLASSESPATH_DIR
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class CasePoolTest {
    lateinit var pool: CasesContainer

    @Before
    fun before() {
        pool = CasesContainer()
    }

    @Test
    fun testForLoadCases() {
        val pool = CasesContainer()
        pool.constructCases(CLASSESPATH_DIR)
        Assert.assertEquals(pool.getRemainCases().size, EXPECTED_NUM_OF_CASE)
        Assert.assertEquals(pool.isAllCasesDispatched(), false)
    }

    @Test
    fun testForDispatchingCases() {
        val cases = arrayListOf<CaseWrapper>()
        val pool = CasesContainer()
        pool.constructCases(CLASSESPATH_DIR)
        pool.constructCases(CLASSESPATH_DIR)

        for (i in 0 until EXPECTED_NUM_OF_CASE * 2) {
            cases.add(pool.getCase())
        }

        Assert.assertEquals(pool.isAllCasesDispatched(), true)
        Assert.assertEquals(cases.size, EXPECTED_NUM_OF_CASE * 2)
    }

    @Test(expected = CaseNotEnoughException::class)
    fun testForGetCaseFromEmptyCasePool() {
        pool.getCase()
    }

    companion object {
        private const val EXPECTED_NUM_OF_CASE = 4
    }
}
