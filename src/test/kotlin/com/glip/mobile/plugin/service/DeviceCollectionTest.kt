package com.glip.mobile.plugin.service

import com.glip.mobile.plugin.container.DevicesContainer
import com.glip.mobile.plugin.model.DeviceWrapper
import com.glip.mobile.testlib.factory.DeviceFactory
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

class DeviceCollectionTest {
    lateinit var container: DevicesContainer
    lateinit var deviceFactory: DeviceFactory
    var singleExcutor = Executors.newSingleThreadExecutor()

    @Before
    fun before() {
        container = DevicesContainer()
        deviceFactory = DeviceFactory()
    }

    @Test
    fun deviceCollectionCanLoadDevicesSuccessfully() {
        val deviceNum = 10
        val deviceList = deviceFactory.getDevices(deviceNum)

        container.loadDevices(deviceList)
        Assert.assertEquals(container.availableDevices.size, deviceNum)
        Assert.assertEquals(container.devices.size, deviceNum)
    }

    @Test
    fun testForGetDevice() {
        val deviceNum = 10
        val deviceList = deviceFactory.getDevices(deviceNum)

        container.loadDevices(deviceList)

        val releaseNum = 4
        val releaseDevices = arrayListOf<DeviceWrapper>()

        for (i in 0 until releaseNum) {
            releaseDevices.add(container.getDevice())
        }

        Assert.assertEquals(releaseDevices.size, releaseNum)
        Assert.assertEquals(container.availableDevices.size, deviceNum - releaseNum)
    }

    @Test(expected = TimeoutException::class)
    fun testForGetDeviceFromEmptyCollection() {
        val future = singleExcutor.submit {
            container.getDevice()
        }

        future.get(2, TimeUnit.SECONDS)
    }

    @Test(expected = TimeoutException::class)
    fun testForGetDeviceFromEmptyCollectionAfterGetting() {
        val deviceNum = 10
        val deviceList = deviceFactory.getDevices(deviceNum)

        container.loadDevices(deviceList)

        val releaseNum = 10

        for (i in 0 until releaseNum) {
            container.getDevice()
        }

        val future = singleExcutor.submit {
            container.getDevice()
        }

        future.get(2, TimeUnit.SECONDS)
    }

    @Test
    fun deviceCollectionCanReleaseDevice() {
        val deviceNum = 10
        val deviceList = deviceFactory.getDevices(deviceNum)

        container.loadDevices(deviceList)
        container.releaseDevice(deviceList.get(0))
        container.releaseDevice(deviceList.get(0))
        Assert.assertEquals(container.availableDevices.size, deviceNum + 2)
    }

    @Test(expected = IllegalArgumentException::class)
    fun deviceCollectionWillThrowExceptionWhenLoadEmptyList() {
        container.loadDevices(listOf())
    }
}