package com.glip.mobile.plugin.service

import com.glip.mobile.plugin.util.CommandLineUtil
import org.junit.Assert
import org.junit.Test

class DeviceServiceTest {
    companion object

    @Test
    fun testCommandLineHelperRunCmdSuccessfully() {
        Assert.assertArrayEquals(CommandLineUtil.runCmd("echo test cmd"), arrayOf("test cmd"))
        Assert.assertArrayEquals(CommandLineUtil.runCmd("echo test cmd"), arrayOf("test cmd"))
    }
}
