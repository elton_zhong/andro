package com.glip.mobile.plugin.tasks

import org.junit.Assert
import org.junit.Test

class DispatchTaskConfigTest {

    var config = createConfig()

    companion object {
        fun createConfig(): DispatchTaskConfig {
            val config = DispatchTaskConfig.getOrCreateConfig("flavor1", "buildType1")
            config.testApkPath = "testApkPath1"
            config.testVariant = "testVariant1"
            config.classesPath = "classpath1"
            config.reportDir = "class"
            config.apkPath = "apkPath"

            return config
        }
    }

    @Test
    fun testForConfigProperties() {
        Assert.assertEquals("flavor1BuildType1", config.variant)
        Assert.assertEquals("assembleFlavor1BuildType1AndroidTest", config.assembleAndroidTestTaskName)
        Assert.assertEquals("assembleFlavor1BuildType1", config.assembleTaskName)
    }

    @Test
    fun testForGetOrCreateConfig() {
        val beforeSize = DispatchTaskConfig.all.size
        DispatchTaskConfig.getOrCreateConfig("f1", "b1")
        DispatchTaskConfig.getOrCreateConfig("f2", "b2")
        val config3 = DispatchTaskConfig.getOrCreateConfig("f3", "b3")
        val anoConfig3 = DispatchTaskConfig.getOrCreateConfig("f3", "b3")

        val afterSize = DispatchTaskConfig.all.size
        Assert.assertEquals(beforeSize + 3, afterSize)
        Assert.assertTrue(config3 == anoConfig3)
    }

    @Test
    fun testForGetConfig() {
        Assert.assertNull(DispatchTaskConfig.getConfig("g1", "g1"))
        Assert.assertNull(DispatchTaskConfig.getConfig("g2", "g2"))

        Assert.assertNotNull(config)
        Assert.assertNotNull(DispatchTaskConfig.getConfig("flavor1", "buildType1"))
        Assert.assertEquals(config, DispatchTaskConfig.getConfig("flavor1", "buildType1"))
    }
}