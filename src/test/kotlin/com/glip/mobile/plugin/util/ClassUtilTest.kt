package com.glip.mobile.plugin.util

import com.glip.andro.annotations.ReInstall
import com.glip.andro.annotations.TestCase
import com.glip.mobile.plugin.container.AnnotationNotFoundException
import com.glip.mobile.plugin.container.CasesContainer
import com.glip.mobile.testlib.factory.ConfigFactory
import org.junit.Assert
import org.junit.Test

class ClassUtilTest {
    val util = ClassPoolUtil.getInstance()

    @Test
    fun `Util is able to Load cases`() {
        Assert.assertNotEquals(0, util.loadClassesByClasspath(ConfigFactory.CLASSESPATH_DIR).size)
    }

    @Test
    fun `Util is able to get TestCase annotation detail`() {
        val container = CasesContainer()
        container.constructCases(ConfigFactory.CLASSESPATH_DIR)
        container.getRemainCases()[0].let {
            val ano = util.getClassAnnotationInstance(it.classPathOfCase, TestCase::class.java)
            Assert.assertEquals(ano.desc, "User can set presence successfully")
            Assert.assertNotNull(ano.id)
            Assert.assertNotNull(ano.url)
        }
    }

    @Test(expected = AnnotationNotFoundException::class)
    fun `Util throw exception when getting null annotation`() {
        val container = CasesContainer()
        container.constructCases(ConfigFactory.CLASSESPATH_DIR)
        container.getRemainCases()[0].let {
            util.getClassAnnotationInstance(it.classPathOfCase, ReInstall::class.java)
        }
    }
}
