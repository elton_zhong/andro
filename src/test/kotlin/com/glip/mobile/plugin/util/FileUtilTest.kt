package com.glip.mobile.plugin.util

import org.apache.commons.lang3.StringUtils
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.File

class FileUtilTest {
    val basicDir = "src/test/resources/testdir"
    val basicDirFile = File(basicDir)

    val dir1 = "d1"
    val pathOfDir1 = getPathOfDir(dir1)
    val dir1File = File(pathOfDir1)

    val dir2 = "d2"
    val pathOfDir2 = getPathOfDir(dir1, dir2)
    val dir2File = File(pathOfDir2)

    val dir3 = "d3"
    val pathOfDir3 = getPathOfDir(dir1, dir2, dir3)
    val dir3File = File(pathOfDir3)

    val emptyDir = ""

    @Before
    @After
    fun cleanBasicDirWorksapce() {
        val f = File(basicDir)
        FileUtil.removeDirectoryRecursively(f)
        Assert.assertEquals(false, f.exists())
        FileUtil.makdirRecursively(f)
        Assert.assertEquals(true, f.exists())
    }

    @Test
    fun testMakdirRecursively() {
        Assert.assertEquals(false, dir3File.exists())

        FileUtil.makdirRecursively(dir3File)
        Assert.assertEquals(dir1File.exists(), true)
        Assert.assertEquals(dir2File.exists(), true)
        Assert.assertEquals(dir3File.exists(), true)
    }

    @Test
    fun testForJoinFile() {
        val joinedFile = FileUtil.join(basicDirFile, "f1", "f2")
        Assert.assertEquals(joinedFile.path, "src/test/resources/testdir/f1/f2")
    }

    @Test
    fun testJoinWithEmptyDir() {
        val joinedFile1 = FileUtil.join(basicDirFile, "f1", emptyDir)
        Assert.assertEquals(joinedFile1.path, "src/test/resources/testdir/f1")

        val joinedFile2 = FileUtil.join(basicDirFile, "f1", emptyDir, "f3")
        Assert.assertEquals(joinedFile2.path, "src/test/resources/testdir/f1/f3")

        val joinedFile3 = FileUtil.join(basicDirFile, "f1", "f2", emptyDir)
        Assert.assertEquals(joinedFile3.path, "src/test/resources/testdir/f1/f2")

        val joinedFile4 = FileUtil.join(basicDirFile, emptyDir, emptyDir, "f1", emptyDir, "f2", emptyDir)
        Assert.assertEquals(joinedFile4.path, "src/test/resources/testdir/f1/f2")
    }

    @Test
    fun remakeDirUnderPathTest() {
        FileUtil.makdirRecursively(dir3File)
        Assert.assertEquals(dir3File.exists(), true)
        FileUtil.remakeDirUnderPath(dir1File, "f4", "f5")
        Assert.assertEquals(true, dir1File.exists())
        Assert.assertEquals(false, dir2File.exists())
        Assert.assertEquals(false, dir3File.exists())

        Assert.assertEquals(true, File("src/test/resources/testdir/d1/f4/f5").exists())

        FileUtil.remakeDirUnderPath(dir2File)
        Assert.assertEquals(true, dir2File.exists())
        Assert.assertEquals(false, dir3File.exists())
    }

    @Test
    fun testJoin() {
        Assert.assertEquals("a/b/c/d", FileUtil.join("a", "b", "", "c", "d", ""))
        Assert.assertEquals("a", FileUtil.join("", "", "a", "", "", ""))
        Assert.assertEquals("", FileUtil.join())
    }

    @Test
    fun testCreateFile() {
        FileUtil.createFileIfFileNotExist(File("src/test/resources/testdir/d0/f0/f5"))
        Assert.assertEquals(true, File("src/test/resources/testdir/d0/f0/f5").exists())
        Assert.assertEquals(true, File("src/test/resources/testdir/d0/f0/f5").isFile())
        Assert.assertEquals(true, File("src/test/resources/testdir/d0/f0/").isDirectory())

        FileUtil.createFileIfFileNotExist(File("src/test/resources/testdir/d0/f0/f6"))
        Assert.assertEquals(true, File("src/test/resources/testdir/d0/f0/f6").exists())
        Assert.assertEquals(true, File("src/test/resources/testdir/d0/f0/f6").isFile())
        Assert.assertEquals(true, File("src/test/resources/testdir/d0/f0/").isDirectory())

        FileUtil.createFileIfFileNotExist(File("src/test/resources/testdir/d0/f0/f6"))
        Assert.assertEquals(true, File("src/test/resources/testdir/d0/f0/f5").exists())
        Assert.assertEquals(true, File("src/test/resources/testdir/d0/f0/f5").isFile())
        Assert.assertEquals(true, File("src/test/resources/testdir/d0/f0/").isDirectory())
    }

    @Test
    fun `test formoveFilesUnderDirToDir`() {
        FileUtil.createFileIfFileNotExist(File("src/test/resources/testdir/d0/f0/1"))
        FileUtil.createFileIfFileNotExist(File("src/test/resources/testdir/d0/f0/2"))
        FileUtil.createFileIfFileNotExist(File("src/test/resources/testdir/d0/f0/0"))
        FileUtil.moveFilesUnderDirToDir(
            File("src/test/resources/testdir/d0/f0/"),
            File("src/test/resources/testdir")
        )
        Assert.assertEquals(true, File("src/test/resources/testdir/0").exists())
        Assert.assertEquals(true, File("src/test/resources/testdir/1").exists())
        Assert.assertEquals(true, File("src/test/resources/testdir/2").exists())
    }

    /**
     * Use a simple joiner, without empty check
     */
    private fun getPathOfDir(vararg dir: String): String {
        return StringUtils.join(listOf(basicDir, *dir), File.separator)
    }
}
