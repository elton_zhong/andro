package com.glip.mobile.plugin.util

import com.glip.mobile.plugin.ConfigurableVariables
import org.apache.logging.log4j.LogManager
import org.junit.AfterClass
import org.junit.Assert
import org.junit.BeforeClass
import org.junit.Test
import java.io.File

class LogUtilTest {

    @Test
    fun fileLogTest() {
        val oldFilePath = LogUtil.logPath
        Assert.assertEquals(true, File(LogUtil.logPath).exists())

        logger.info("1234test")
        Assert.assertEquals(false, File(configurableVariables.fileLogPath).exists())

        LogUtil.changeFileLoggerOutputTo(configurableVariables.fileLogPath)
        logger.info("ggtest")

        Assert.assertEquals(true, File(configurableVariables.fileLogPath).exists())
        Assert.assertEquals(configurableVariables.fileLogPath, LogUtil.logPath)
        Assert.assertEquals(true, File(LogUtil.logPath).exists())
        Assert.assertEquals(false, File(oldFilePath).exists())
        Assert.assertNotEquals(oldFilePath, configurableVariables.fileLogPath)

        val logContent = CommandLineUtil.runCmdAndGetString(
            "tail %s",
            File(configurableVariables.fileLogPath).absolutePath
        )

        Assert.assertTrue(logContent.contains("1234test"))
        Assert.assertTrue(logContent.contains("ggtest"))
    }

    @Test
    fun testAdapter() {
        val log = LogUtil.getAdapter(logger)
        log.info("Starting %1\$d tests on %2\$s", 1, "device")
        log.info("ada%stest2", "2")

        val logContent = CommandLineUtil.runCmdAndGetString(
            "tail %s",
            File(LogUtil.logPath).absolutePath
        )

        Assert.assertTrue(logContent.contains("Starting 1 tests on device"))
        Assert.assertFalse(logContent.contains("ada%stest2"))
    }

    companion object {
        val logger = LogManager.getLogger(LogUtilTest::class.java)
        private val configurableVariables = ConfigurableVariables.get()

        @JvmStatic
        @BeforeClass
        fun cleanBasicDirWorksapce() {
            cleanWorkspace()
            val logFile = File(configurableVariables.fileLogDir)
            FileUtil.makdirRecursively(logFile)

            Assert.assertEquals("src/test/resources/testlogdir/log", logFile.path)
            Assert.assertEquals(true, logFile.exists())
        }

        @JvmStatic
        @AfterClass
        fun cleanWorkspace() {
            configurableVariables.reportDir = "src/test/resources/testlogdir"
            FileUtil.removeDirectoryRecursively(File(configurableVariables.reportDir))
        }
    }
}
