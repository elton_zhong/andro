package com.glip.mobile.testlib.factory

import com.glip.mobile.plugin.model.CaseWrapper
import com.nhaarman.mockitokotlin2.doNothing
import com.nhaarman.mockitokotlin2.doReturn
import com.nhaarman.mockitokotlin2.mock
import com.nhaarman.mockitokotlin2.spy

class CaseFactory {
    fun getCases(num: Int): List<CaseWrapper> {
        return (0 until num).map { getCase() }
    }

    fun getCase(): CaseWrapper {
        val case = mock<CaseWrapper> {
            onGeneric { isNeedTobeReInstall } doReturn false
        }
        case.casesContainer = spy {}
        doNothing().`when`(case.casesContainer).finishCase(case)
        doReturn(null).`when`(case).reInstall

        return case
    }
}
