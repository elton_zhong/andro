package com.glip.mobile.testlib.factory

import com.glip.mobile.plugin.tasks.DispatchTaskConfig
import com.glip.mobile.plugin.util.CommandLineUtil
import org.apache.logging.log4j.LogManager
import java.io.File

class ConfigFactory {
    fun getConfig(): DispatchTaskConfig {
        val config = DispatchTaskConfig.getOrCreateConfig("inhouse", "debug")

        config.apkPath = APK_PATH
        config.classesPath = CLASSESPATH_DIR
        config.reportDir = REPORT_DIR
        config.testApkPath = TEST_APK_PATH
        config.testVariant = "inhousedebug"

        return config
    }

    companion object {
        const val CLASSESPATH_DIR = "src/test/resources/testclasses/inhouseDebugAndroidTest"
        const val APK_PATH = "src/test/resources/apks/glip-5.8.0.1.000-xmn-up-inhouse-debug.apk"
        const val REPORT_DIR = "src/test/resources/report/inhouse/debug"
        const val TEST_APK_PATH = "src/test/resources/apks/glip-5.8.0.1.000-xmn-up-inhouse-debug-androidTest.apk"
        const val APK_ADDRESS =
            "http://10.32.52.92/manual/resources/caseWrapper-dispatcher/glip-5.8.0.1.000-xmn-up-inhouse-debug.apk"
        const val TEST_APK_ADDRESS =
            "http://10.32.52.92/manual/resources/caseWrapper-dispatcher/glip-5.8.0.1.000-xmn-up-inhouse-debug-androidTest.apk"
        val logger = LogManager.getLogger(ConfigFactory::class.java)!!
    }

    fun prepareWorkspace() {
        downloadIfFileNotExists(APK_ADDRESS, APK_PATH)
        downloadIfFileNotExists(TEST_APK_ADDRESS, TEST_APK_PATH)
    }

    private fun downloadIfFileNotExists(url: String, path: String) {
        if (!File(path).exists()) {
            downloadFromUrlToDir(url, path)
            return
        }
        logger.info("File $path already exists")
    }

    private fun downloadFromUrlToDir(url: String, path: String) {
        CommandLineUtil.runCmd("wget -O %s %s", File(path).absolutePath, url)
    }
}
