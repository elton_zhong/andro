package com.glip.mobile.testlib.factory

import com.glip.mobile.plugin.model.DeviceWrapper
import com.nhaarman.mockitokotlin2.doAnswer
import com.nhaarman.mockitokotlin2.doNothing
import com.nhaarman.mockitokotlin2.mock

class DeviceFactory {
    fun getDevices(num: Int): List<DeviceWrapper> {
        return (0 until num).map { getDevice() }
    }

    fun getDevice(): DeviceWrapper {
        val m = mock<DeviceWrapper> {}
        doNothing().`when`(m).reInstall()
        doAnswer { return@doAnswer }.`when`(m).prepare()
        return m
    }
}